# Upgrading Opencast From 2.2 To 2.3

This guide describes how to upgrade Opencast 2.2.x to 2.3.x. In case you need information about how to upgrade older
versions of Opencast, please refer to the [old release notes](https://docs.opencast.org).

## Configuration Changes

* The activemq configuration must be updated. You can find the latest configuration under `docs/scripts/activemq/activemq.xml`.